package RestAssured;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;


public class GetData {
    @Test
    public void testResponsecode()
    {
        Response resp=RestAssured.get("https://yandex.ru/pogoda/moscow/details?lat=55.74258733228377&lon=37.578192875422644");

        int code=resp.getStatusCode();

        System.out.println("Status code" + code);



        Assert.assertEquals(code, 200);


    }
    @Test
    public void testBody()
    {
        Response resp=RestAssured.get("https://yandex.ru/pogoda/moscow/details?lat=55.74258733228377&lon=37.578192875422644");


        String data=resp.asString();

        System.out.println("Data" + data);
        //System.out.println("Response time" + resp.getTime());



    }

    @Test
    public void testBodyHeader()
    {

        Response resp=RestAssured.get("https://yandex.ru/pogoda/moscow/details?lat=55.74258733228377&lon=37.578192875422644");


        String data=resp.asString();

        System.out.println("Data" + data);
        System.out.println("Response time" + resp.getTime());



    }


}
