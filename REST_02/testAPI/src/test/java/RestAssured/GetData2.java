package RestAssured;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.ResponseBody;
import org.hamcrest.Matcher;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import io.restassured.response.Response;

import javax.xml.ws.Endpoint;

import static org.hamcrest.CoreMatchers.*;




public class GetData2 {

    @Test
    public void testBodyHeader()
    {

        String baseURL = "https://api.weather.yandex.ru/v1/forecast";
        Response resp = (Response) RestAssured.given().
                header("X-Yandex-API-Key","0cd57e30-a241-4157-9f1c-ddd1ef6a9762").
                queryParam("lat",55.75396).and().
                queryParam("lon",37.620393).and().
                queryParam("extra",true).
                contentType("application/json").
                get(baseURL).then().log().all().assertThat().statusCode(200);

        resp.then().body("now", equalTo(1570807187));

        //String body = resp.asString();
        //System.out.println("Response " +body);

    }
    @Test
    public void testBodyHeader2()
    {
        String url = "https://api.weather.yandex.ru/v1/forecast?lat=55.75396&lon=37.620393&extra=true";

        given().
                header("X-Yandex-API-Key","0cd57e30-a241-4157-9f1c-ddd1ef6a9762").
                get(url).
                then().
                statusCode(200).
                log().
                all();







    }





}
