package com.fluentWait.framework;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeSuite;
import static io.restassured.RestAssured.given;

public class RestAssuredConfiguration {

    @BeforeSuite(alwaysRun = true)
    public void configureRestAssured(){
        RestAssured.baseURI = "https://api.weather.yandex.ru/v1/forecast";
        //RestAssured.basePath = "lat=55.75396&lon=37.620393&extra=true";
        RestAssured.requestSpecification = given().
                header("X-Yandex-API-Key","0cd57e30-a241-4157-9f1c-ddd1ef6a9762");

    }
    public RequestSpecification getRequestSpecification(){
        return RestAssured.given().contentType(ContentType.JSON);
        //return RestAssured.given().log().all();
    }
}
