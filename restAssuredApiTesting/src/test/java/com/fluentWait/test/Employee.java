package com.fluentWait.test;
import com.fluentWait.framework.RestAssuredConfiguration;
import com.fluentWait.test.common.EndPoint;
import io.restassured.RestAssured;
import static org.testng.Assert.*;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.ws.Endpoint;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class Employee {
    String GET_HEADER_NAME = "X-Yandex-API-Key";
    String GET_HEDER_VALUE = "0cd57e30-a241-4157-9f1c-ddd1ef6a9762";
    String GET_EMPLOYEE = "lat=55.75396&lon=37.620393&extra=true";
    String GET_BASE_URL = "https://api.weather.yandex.ru/v1/forecast?lat=55.75396&lon=37.620393&extra=true";

    @Test
    public void validateEmployee(){
        given().
                header("X-Yandex-API-Key","0cd57e30-a241-4157-9f1c-ddd1ef6a9762").
                get("https://api.weather.yandex.ru/v1/forecast?lat=55.75396&lon=37.620393&extra=true").
                then().
                statusCode(200).
                log().
                all();

    }

    @Test(groups = "demo")
    public void validatePathParameterLat(){

        given().
                header(GET_HEADER_NAME,GET_HEDER_VALUE).
                get(GET_BASE_URL).
                then().
                assertThat().
                statusCode(200).
                log().
                all();




    }

    @Test(groups = "demo")
    public void validatePathParameter02(){

        given().
                header(GET_HEADER_NAME,GET_HEDER_VALUE).
                get(GET_BASE_URL).
                then().
                statusCode(200).
                body("lat", equalTo(55.75396)).
                extract().
                response();




    }


    @Test(groups = "demo")
    public void validatePathParameter4(){

        RequestSpecification requestSpecification = new RestAssuredConfiguration().getRequestSpecification();
        requestSpecification.header(EndPoint.GET_HEADER_NAME,EndPoint.GET_HEDER_VALUE).
                queryParam("lat",55.75396).and().
                queryParam("lon",37.620393).and().
                queryParam("extra", true).
                then().
                body("lat", equalTo(55.75396)).response();
        given().spec(requestSpecification).then().statusCode(200).log().all();




        //Response response = given().spec(requestSpecification).get(EndPoint.GET_ALL_PATH);
        //System.out.println("response " +response);
        //response.then().body("lat", equalTo(55.75396));



    }


}
